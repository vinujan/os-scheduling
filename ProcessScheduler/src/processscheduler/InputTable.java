/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processscheduler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author vinu
 */
public class InputTable implements ActionListener {
    private JTable inputTable;
    private boolean isCan;
    JFrame input;
    private ArrayList<Process> process;
    private int totalTime;
    private int totalProcess;
    private ProcessScheduler ps;
    
    public InputTable(int totalProcess,ProcessScheduler ps){
        totalTime = 0;
        isCan = true;
        process  = new ArrayList<Process>();
        input = new JFrame();
        this.ps = ps;
        this.totalProcess = totalProcess;
        JScrollPane jspInput = new JScrollPane();
        JButton ok;
        ///
        jspInput = new javax.swing.JScrollPane();
        inputTable = new javax.swing.JTable();
        ok = new javax.swing.JButton();

        input.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        inputTable.setModel(new javax.swing.table.DefaultTableModel(
            new String [] {
                "Process Name", "Start Time", "Burst Time"
            },
            totalProcess
        ));
        jspInput.setViewportView(inputTable);

        ok.setText("Ok");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(input.getContentPane());
        input.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jspInput , javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(153, 153, 153)
                .addComponent(ok)
                .addGap(0, 174, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jspInput , javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ok)
                .addContainerGap())
        );
        ////////////////////////////////////////////////////////////////////////////
        ok.addActionListener(this);
        input.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        input.pack();
        input.setVisible(true);
    }
    public JTable getTable(){
        return inputTable;
    }
    public boolean getStatus(){
        return isCan;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for(int i = 0; i < totalProcess;i++){
            process.add(new Process((String)inputTable.getValueAt(i, 0),
                        Integer.parseInt((String)inputTable.getValueAt(i, 1)),
                        Integer.parseInt((String)inputTable.getValueAt(i, 2)),ps));
            totalTime= totalTime + Integer.parseInt((String)inputTable.getValueAt(i, 2));
        }
        isCan = false;
        input.dispose();
    }
    public int getTotalTime(){
        return totalTime;
    }
    
    public ArrayList<Process> getProcessArray(){
        return process;
    }
}
