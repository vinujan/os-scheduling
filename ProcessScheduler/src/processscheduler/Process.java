/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processscheduler;

/**
 *
 * @author vinu
 */
public class Process {
    private String name;
    private String status;
    private final int arrivalTime;
    private final int burstTime;
    private ProcessScheduler ps;

    public Process(String name,int aTime, int bTime,ProcessScheduler ps){
        status = "ready";
        this.name = name;
        arrivalTime = aTime;
        burstTime = bTime;
        this.ps = ps;
    }
    public String getName(){
        return name;
    }
    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getBurstTime() {
        return burstTime;
    }
    
    public void setProcessScheduler(ProcessScheduler ps){
        this.ps = ps;
    }
    
    public int getWaitingTime(){
        return ps.getTime() - getArrivalTime();
    }
    
    public double getHRRN(){
        if (getWaitingTime() < 0){
            return 0;
        }
        else
        return 1 + 1.0*getWaitingTime()/getBurstTime();
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
