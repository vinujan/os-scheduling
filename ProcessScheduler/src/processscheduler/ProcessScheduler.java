/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processscheduler;

import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vinu
 */
public class ProcessScheduler {
    private int time;
    private ArrayList<Process> process;
    /**
     * @param args the command line arguments
     */
    public ProcessScheduler(){
        time = 0;
        process = new ArrayList<Process>();
    }
    
    public int getTime(){
        return time;
    }
    
    public void addProcess(ArrayList<Process> p){
        process = p;
    }
    public ArrayList<Process> getProcess(){
        return process;
    }
    
    public String[] getNextProcess(){
        String[] ret = new String[process.size() + 1];
        double temp = 0;
        double temp2 = 0;
        int temp3 = 0;
        for(int i = 0;i < process.size();i++){
            if(process.get(i).getStatus() == "complete"){
                ret[i + 1] = "complete";
            }
            else{
                temp2 = process.get(i).getHRRN();
                if(temp2 == 0){
                    ret[i + 1] = "ready";
                    continue;
                }
                if(temp2 > temp){
                    ret[i + 1]  = ""+temp2;
                    temp = temp2;
                    temp3 = i; 
                }
                else
                    ret[i+1] = ""+temp2;
            }
            
        }
        process.get(temp3).setStatus("complete");
        time = time + process.get(temp3).getBurstTime();
        ret[0] = "" + temp3;
        ret[temp3 + 1] = ret[temp3 + 1] + " - executing " ;
        return ret;
    }
}
